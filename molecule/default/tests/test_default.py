import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('crss')


def test_crss_index(host):
    cmd = host.command("curl -H Host:ics-ans-crss-default -k -L https://localhost")
    assert "<title>CRSetup</title>" in cmd.stdout
